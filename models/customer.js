const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({

    _id: {
        type: Number,
        required: true,
        description: "Unique identifier"
    },
    
    firstname: {
        type: String,
        required: true,
        default: 'karthik'
       
    },
    lastname: {
        type: String,
        required: true,
        default: 'b'
       
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    age: {
        type: Number,
        required: true
       
    },
    street: {
        type: String,
        required: true,
        default: 'Street'
    },
    city: {
        type: String,
        required: true,
        default: 'Maryville'
    },
    state: {
        type: String,
        required: true,
        default: 'MO'
    },
    country: {
        type: String,
        required: true,
        default: 'USA'
    },
    zip: {
        type: String,
        required: true,
        default: '64468'
    }




})
module.exports = mongoose.model('Customer', CustomerSchema)