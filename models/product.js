const mongoose =require('mongoose');

const ProductSchema = new mongoose.Schema({

    _id: 
    { type: Number, 
      required: true 
    },

    productKey: {
      type: String,
      required: true,
      unique: true,
      default: 'product'
    },
    
    pricePerUnit: {
      type: Number,
      required: true,
      default: 9.99,
      min: 0,
      max: 100000
    },
    quantity: {
        type:Number,
        required:true,
        min:0,
        max:500
    },
    productdescription: {
        type: String,
        required: false,
        default: 'description'
      },
    ratings: {
        type:Number,
        required:true,
        min:0,
        max:5
    }
})
  module.exports = mongoose.model('Product', ProductSchema)