const express = require("express");
const LOG = require("../utils/logger.js");


LOG.debug("START routing");
const router = express.Router();

// Manage top-level request first

router.get("/", function (req, res) {
  
  res.render("index.ejs")
 });

 router.get("/customer", function (req, res) {

  res.render("customer/index.ejs")
 });

 router.get("/product", function (req, res) {

  res.render("product/index.ejs")
 });


router.get("/orders", function (req, res) {
  res.render("orders/index.ejs")
 });
 
 router.get("/orderLineItem", function (req, res) {

  res.render("orderLineItem/index.ejs")
 });
// Defer path requests to a particular controller
router.use("/customer", require("../controllers/customer.js"));
router.use("/product", require("../controllers/product.js"));
router.use("/orders", require("../controllers/orders.js"));

router.use("/orderLineItems", require("../controllers/orderLineItem.js"));

LOG.debug("END routing");
module.exports = router;